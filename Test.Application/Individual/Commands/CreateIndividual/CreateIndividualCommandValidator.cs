﻿using FluentValidation;


namespace Test.Application.Individual.Commands.CreateIndividual
{
    public class CreateIndidvidualCommandValidator : AbstractValidator<CreateIndividualCommand>
    {
        public CreateIndidvidualCommandValidator()
        {
            // валидация пример
            RuleFor(createNoteCommand =>
                createNoteCommand.INN).Matches("^\\d{10}$|^\\d{12}$");
        }
    }
}
