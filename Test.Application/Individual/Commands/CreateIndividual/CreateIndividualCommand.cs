﻿using MediatR;
using Microsoft.AspNetCore.Http;

namespace Test.Application.Individual.Commands.CreateIndividual
{
    public class CreateIndividualCommand : IRequest<Guid>
    {       
        public string INN { get; set; }      
        public IFormFile fileINN { get; set; }       
        public string OGRN { get; set; }       
        public IFormFile fileOGRN { get; set; }       
        public DateTime RegDate { get; set; }      
        public IFormFile fileEGRIP { get; set; }
        public IFormFile fileContarct { get; set; }
    }   
}
