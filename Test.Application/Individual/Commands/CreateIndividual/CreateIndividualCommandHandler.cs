﻿using MediatR;


namespace Test.Application.Individual.Commands.CreateIndividual
{
    internal class CreateIndividualCommandHandler
         : IRequestHandler<CreateIndividualCommand, Guid>
    {

        public async Task<Guid> Handle(CreateIndividualCommand request,
            CancellationToken cancellationToken)
        {
            // Пишем в базу
            //await _dbContext.Notes.AddAsync(item, cancellationToken);
            //await _dbContext.SaveChangesAsync(cancellationToken);

            return new Guid();
        }
    }
}