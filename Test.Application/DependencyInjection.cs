﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Test.Application.Common.Behaviors;

namespace Test.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(
            this IServiceCollection services)
        {          
            services.AddMediatR(cf => cf.RegisterServicesFromAssembly(typeof(DependencyInjection).Assembly));         

            services.AddTransient(typeof(IPipelineBehavior<,>),
                typeof(ValidationBehavior<,>));           

            return services;
        }
    }
}
