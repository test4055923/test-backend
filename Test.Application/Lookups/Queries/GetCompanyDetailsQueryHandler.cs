﻿
using MediatR;
using Test.Application.Common.Exceptions;

namespace Test.Application.Lookups.Queries
{ 
    public class GetCompanyDetailsQueryHandler
       : IRequestHandler<GetCompnyDetailsQuery, CompanyDetailsVm>
    {      

        public async Task<CompanyDetailsVm> Handle(GetCompnyDetailsQuery request,
            CancellationToken cancellationToken)
        {
            // запрос в бд при наличии
            //var entity = await _dbContext.Company
            //   .FirstOrDefaultAsync(company =>
            //   company.INN == request.INN, cancellationToken);

            if (request.INN != "2680466569")
            {
                throw new NotFoundException("Company", request.INN) ;
            }
            return new CompanyDetailsVm { CompanyName = "Test Company", CompanyShortName = "OOO TC", RegDate = new DateTime(), OGRN = "50804024438" };            
        }
    }
}
