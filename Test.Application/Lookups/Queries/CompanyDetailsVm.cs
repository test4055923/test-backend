﻿
namespace Test.Application.Lookups.Queries
{
    public class CompanyDetailsVm
    {       
        public string CompanyName { get; set; }
        public string CompanyShortName { get; set; }
        public DateTime RegDate { get; set; }
        public string OGRN { get; set; }        
    }
}
