﻿using MediatR;
namespace Test.Application.Lookups.Queries
{
    public class GetCompnyDetailsQuery : IRequest<CompanyDetailsVm>
    {
        public string INN { get; set; }
    }
}
