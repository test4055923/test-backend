﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Test.Api.Models;
using Test.Application.Individual.Commands.CreateIndividual;

namespace Test.Api.Controllers
{    
    public class IndividualController : BaseController
    {

        private readonly IMapper _mapper;


        public IndividualController(IMapper mapper) => _mapper = mapper;

        /// <summary>
        /// Сохранение индивидуального предпринимателя
        /// </summary>
        /// <param name="createIndividualDto">CreateIndividualDto object</param>
        /// <response code="200">OK</response>
        /// <returns>Returns AuthenticationResult</returns>
        [HttpPost]
        [Route("")]
        [RequestSizeLimit(5 * 1024 * 1024)]
        public async Task<ActionResult<Guid>> Create([FromForm] CreateIndividualDto createIndividualDto)
        {
            var command = _mapper.Map<CreateIndividualCommand>(createIndividualDto);
            var rtn = await Mediator.Send(command);
            return Ok(rtn);
        }
    }
}
