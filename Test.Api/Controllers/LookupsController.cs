﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Test.Application.Lookups.Queries;

namespace Test.Api.Controllers
{
    [Produces("application/json")]
    public class Lookups : BaseController
    {

        private readonly IMapper _mapper;


        public Lookups(IMapper mapper) => _mapper = mapper;

        /// <summary>
        /// Gets company information by INN
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /lookups/2680466569
        /// </remarks>
        /// <param name="id">INN id (string)</param>
        /// <returns>Returns CompanyDetailsVm</returns>
        /// <response code="200">Success</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]     
        public async Task<ActionResult<CompanyDetailsVm>> Get(string id)
        {
            var query = new GetCompnyDetailsQuery
            {
                INN = id
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }
    }
}
