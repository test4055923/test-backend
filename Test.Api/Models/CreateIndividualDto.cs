﻿using System.ComponentModel.DataAnnotations;
using Test.Application.Common.Mappings;
using Test.Application.Individual.Commands.CreateIndividual;

namespace Test.Api.Models
{
    public class CreateIndividualDto : IMapWith<CreateIndividualCommand>
    {
        [Required]
        public string INN { get; set; }
        [Required]
        public IFormFile fileINN { get; set; }
        [Required]
        public string OGRN { get; set; }
        [Required]
        public IFormFile fileOGRN { get; set; }
        [Required]
        public DateTime RegDate { get; set; }
        [Required]
        public IFormFile fileEGRIP { get; set; }
        public IFormFile fileContarct { get; set; }
    }
}
